﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Enigma
{
    public class DefaultFileDialog: IFileDialog
    {
        public string Path { get; set; }
        public bool SaveFile(string text, string path)
        {
            if (!(Path == null || Path == ""))
            {
                FileInfo info = new FileInfo(path);

                if (info.Exists == false)
                {
                    try
                    {
                        File.WriteAllText(path + ".txt", text, Encoding.Default);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    try
                    {
                        File.WriteAllText(path, text, Encoding.Default);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                MessageBox.Show("Путь не был указан");
                return false;
            }
        }

        public string PathFileGet()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "txt files (*.txt)|*.txt"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                Path = openFileDialog.FileName;
            }
            else
            {
                return null;
            }


            FileInfo info = new FileInfo(Path);

            if (info.Exists == false)
            {
                throw new Exception("Файл не может быть прочитан. Путь не существет\nPath: " + Path);
            }
            else
            {
                try
                {
                    return Path;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public string OpenFile(string path)
        {
            if (!(Path == null || Path == ""))
            {
                FileInfo info = new FileInfo(path);

                if (info.Exists == false)
                {
                    throw new Exception("Файл не может быть прочитан. Путь не существет\nPath: " + path);
                }
                else
                {
                    try
                    {
                        return File.ReadAllText(path, Encoding.Default);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                MessageBox.Show("Путь не был указан");
                return "";
            }
        }
    }
}
