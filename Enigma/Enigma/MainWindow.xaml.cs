﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Enigma
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string Key { get; set; }
        public string Message { get; set; }
        public static string EncryptedMessage { get; set; }
        public static string DecryptedMessage { get; set; }

        DefaultFileDialog fileDialog = new DefaultFileDialog();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnCalculate_Click(object sender, RoutedEventArgs e)
        {
            if (!(TbText.Text == "" || TbText.Text == null))
            {
                if (!(TbKey.Text == "" || TbKey.Text == null))
                {
                    char[] message = Message.ToCharArray();
                    char[] key = Key.ToCharArray();

                    if (CbEncrypted.IsChecked == true)
                    {
                        message = Encrypted(message, key);
                        EncryptedMessage = new string(message);
                        TbDecryptedText.Text = EncryptedMessage;

                    }
                    else
                    {
                        message = Decrypted(message, key);
                        DecryptedMessage = new string(message);
                        TbDecryptedText.Text = DecryptedMessage;
                    }
                }
                else
                {
                    MessageBox.Show("Поле \"Ключ\" пустое!"); ;
                }    
            }
            else
            {
                MessageBox.Show("Поле \"Текст\" пустое!");
            }
        }

        public char[] Encrypted(char[] message, char[] key)
        {
            int numWord;
            int d;
            int j, f;
            int numKey = 0;

            char[] alf = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };

            for (int i = 0; i < message.Length; i++)
            {
                for (j = 0; j < alf.Length; j++)
                {
                    if (message[i] == alf[j])
                    {
                        break;
                    }
                }

                if (j != 33)
                {
                    numWord = j;

                    if (numKey > key.Length - 1)
                    {
                        numKey = 0;
                    }

                    for (f = 0; f < alf.Length; f++)
                    {
                        if (key[numKey] == alf[f])
                        {
                            break;
                        }
                    }

                    numKey++;

                    if (f != 33)
                    {
                        d = numWord + f;
                    }
                    else
                    {
                        d = numWord;
                    }
                    if (d > 32)
                    {
                        d = d - 33;
                    }
                    message[i] = alf[d];
                }
            }
            return message;
        }

        public char[] Decrypted(char[] message, char[] key)
        {
            int numWord;
            int d;
            int j, f;
            int numKey = 0;

            char[] alf = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };

            for (int i = 0; i < message.Length; i++)
            {
                for (j = 0; j < alf.Length; j++)
                {
                    if (message[i] == alf[j])
                    {
                        break;
                    }
                }

                if (j != 33)
                {
                    numWord = j;

                    if (numKey > key.Length - 1)
                    {
                        numKey = 0;
                    }

                    for (f = 0; f < alf.Length; f++)
                    {
                        if (key[numKey] == alf[f])
                        {
                            break;
                        }
                    }

                    numKey++;

                    if (f != 33)
                    {
                        d = numWord - f + alf.Length;
                    }
                    else
                    {
                        d = numWord;
                    }
                    if (d > 32)
                    {
                        d = d - 33;
                    }
                    message[i] = alf[d];
                }
            }
            return message;
        }

        private void CbEncrypted_Click(object sender, RoutedEventArgs e)
        {
            CbDecrypted.IsChecked = !CbEncrypted.IsChecked;
        }

        private void CbDecrypted_Click(object sender, RoutedEventArgs e)
        {
            CbEncrypted.IsChecked = !CbDecrypted.IsChecked;
        }

        private void TbKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            Key = TbKey.Text.ToLower();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Message = TbText.Text.ToLower();
            EncryptedMessage = Message;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!((TbText.Text == "" || TbText.Text == null) && (TbDecryptedText.Text == "" || TbDecryptedText.Text == null)))
            {
                MessageFile messageFile = new MessageFile();

                messageFile.Topmost = true;
                messageFile.Show();
            }
            else
            {
                MessageBox.Show("Нечего сохранять");
            }    
        }

        private void BtnOpen_Click(object sender, RoutedEventArgs e)
        {
            fileDialog.Path = fileDialog.PathFileGet();
            Message = fileDialog.OpenFile(fileDialog.Path);
            TbText.Text = Message;
        }

        private void TbDecryptedText_TextChanged(object sender, TextChangedEventArgs e)
        {
            DecryptedMessage = TbDecryptedText.Text;
        }
    }
}
